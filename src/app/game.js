'use client'

import { useState, useEffect } from 'react';

import styles from './page.module.css';
import Star from './star';
import Numbers from './number';
import { randomIntFromInterval } from './utils';

const MIN_STAR = 1;
const MAX_STAR = 9;
const COUNT_NUMBERS = 9;
const INCREMENT_NUMBERS = 1;
const INITIAL_NUMBES_VALUE = 1;

export default function Game() {
    const [starsCount, setStarsCount] = useState(0);

    useEffect(() => {
        setStarsCount(randomIntFromInterval(MIN_STAR, MAX_STAR));
    }, [starsCount]);

    return(
        <div className={styles.game}>
            <div className={styles.help}>
                Pick 1 or more numbers that sum to the number of stars
            </div>
            <div className={styles.body}>
                <div className={styles.left}>
                    <Star count={starsCount}/>
                </div>
                <div className={styles.right}>
                    <Numbers count={COUNT_NUMBERS} increment={INCREMENT_NUMBERS} initialValue={INITIAL_NUMBES_VALUE}/>
                </div>
            </div>
            <div className={styles.timer}>Time Remaining: 10</div>
        </div>
    );
}
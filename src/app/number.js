import styles from './page.module.css';
import { geyArrayFromNumber } from './utils';

export default function Numbers({ count, increment, initialValue }) {
    const buttons = geyArrayFromNumber(count);
    return (
        <>
            {buttons.map((element, index) => <button className={styles.number} key={index}>{initialValue + increment*index}</button>)}
        </>
    );
}
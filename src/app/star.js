import styles from './page.module.css';
import { geyArrayFromNumber } from './utils';

export default function Star({ count }) {
    const stars = geyArrayFromNumber(count);
    return (
        <>{stars.map((element, index) => <div className={styles.star} key={index} />)}</>
    );
}
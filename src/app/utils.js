export function geyArrayFromNumber(number) {
    return Array(number).fill(0);
}

export function randomIntFromInterval(min, max) {
    return Math.floor((Math.random() * (max - min + 1)) + min);
}
